<?php
/**
 * Created by PhpStorm.
 * User: Metalbrain
 * Date: 11.11.2017
 * Time: 21:09
 */

class Interests_model extends CI_Model
{
    public function getAllInterests()
    {
        $this->db->select(['id', 'name']);
        $this->db->from('INTERESTS');
        $query = $this->db->get();
        return $query->result();
    }
}