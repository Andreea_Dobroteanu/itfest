<?php

class Register_model  extends CI_Model{

    public function insert_profile($last_name, $first_name, $email, $pass, $phone, $interests)
    {
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => $pass,
            'phone' => $phone
        );

        $this->db->insert('USERS', $data);
        $uid = $this->db->insert_id();

        $interests = json_decode($interests);

        foreach($interests as $interest_id) {
            $this->db->insert('USERS_INTERESTS', [
                "id_user" => $uid,
                "id_interest" => $interest_id
            ]);
        }

        http_response_code(200);
    }
}