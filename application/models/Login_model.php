<?php

class Login_model extends CI_Model
{
    public function getUser($email, $password)
    {
        $query = $this->db->get_where('USERS', array('email' => $email, 'password' => $password), 1, 0);

        if ($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }
}