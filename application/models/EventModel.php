<?php

class EventModel  extends CI_Model{

    public function event_add($name, $description, $interest, $date, $location, $time, $user)
    {
        $data = array(
            'id' => '',
            'name' => $name,
            'description' => $description,
            'id_interest' => $interest,
            'date' => $date,
            'location' => $location,
            'time' => $time,
            'id_owner' => $user
        );

        $this->db->insert('EVENTS', $data);
    }

    public function event_remove($id_event)
    {
        $this->db->where('id', $id_event);
        $this->db->delete('EVENTS');
    }

    public function event_update($update_array, $id)
    {
        unset($update_array['id']);
        $this->db->where('id', $id);
        $this->db->update('EVENTS', $update_array);
    }

    public function getAllEvents()
    {
        $this->db->select(['id', 'name', 'description', 'id_owner', 'date', 'location']);
//        $this->db->join();
        return $this->db->get('EVENTS')->result_array();
    }

    public function get_events_for_user($id_user){
        $this->db->select("*")->where('id_owner', $id_user)->get('EVENTS');
    }
}