<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: MadalinaM
 * Date: 11/11/2017
 * Time: 4:40 PM
 */

class User_profile_model extends CI_Model {

    function get_user_first_name($id) {
        $this->db->select('first_name');
        $this->db->from('USERS');
        $this->db->where('first_name', $id);
        $query = $this->db->get();
        $name = $query->result();
        $name = empty($name) ? "" : $name[0];
        return $name;
    }

    function get_user_last_name($id) {
        $this->db->select('last_name');
        $this->db->from('USERS');
        $this->db->where('last_name', $id);
        $query = $this->db->get();
        $name = $query->result();
        $name = empty($name) ? "" : $name[0];
        return $name;
    }

    function get_user_profile_pic($id) {
        $this->db->select('profile_pic');
        $this->db->from('USERS');
        $this->db->where('profile_pic', $id);
        $query = $this->db->get();
        $pic = $query->result();
        $pic = empty($name) ? "" : $pic[0];
        return $pic;
    }

    function edit_user_info($id, $first_name, $last_name, $update) {
   ///function edit_user_info($id, $update) {

        $this->db->set('first_name', $first_name);
        $this->db->set('last_name', $last_name);
        $this->db->where('id', $id);
        $this->db->update('USERS');
    }
}
