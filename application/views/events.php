<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="/public/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="/public/css/magnific-popup.css" rel="stylesheet">
    <link href="/public/css/vendor/select2.min.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="/public/css/style.css" rel="stylesheet">
</head>

<body id="page-top">


<?php
    if ($auth) {
        $this->load->view('partials/logged_header');
    }
    else {
        $this->load->view('partials/default_header');
    }
?>


<div class="container events-container" id="events_container">
    <div class="row">
        <div class="col-lg-12">
            <h3>EventizeR.</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <input type="search" id="search" value="" class="form-control" placeholder="Search event...">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table" id="events_list_table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Owner</th>
                    <th>Date</th>
                    <th>Location</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($events as $item) { ?>
                    <tr>
                        <td class="evt-id-td"><?php echo $item["id"] ?></td>
                        <td class="evt-name">
                            <span><?php echo $item["name"] ?></span>
                            <input type="text" class="event-name-edit hidden" value="<?php echo $item["name"] ?>"/>
                        </td>
                        <td class="evt-description">
                            <span><?php echo $item["description"] ?></span>
                        </td>
                        <td class="evt-owner">
                            <span><?php echo $item["id_owner"] ?></span>
                        </td>
                        <td class="evt-date">
                            <span><?php echo $item["date"] ?></span>
                        </td>
                        <td class="location">
                            <span><?php echo $item["location"] ?></span>
                            <input type="text" class="location-edit hidden" value="<?php echo $item["location"] ?>"/>
                        </td>
                        <td class="delete-event" data-id="<?php echo $item["id"] ?>">
                            <a href="#" id="delete_event_button" data-toggle="modal" data-target="#delete_event_modal" class="delete-event-button">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <hr>
        </div>
    </div>
</div>

<a href="#" id="add_job_button" data-toggle="modal" data-target="#add_job_modal" class="add-job-button btn btn-default btn-lg">
    <i class="fa fa-plus"></i>
</a>







<!-- Bootstrap core JavaScript -->
<script src="/public/js/vendor/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.9.4/umd/popper.js"></script>
<script src="/public/js/vendor/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/public/js/vendor/jquery.easing.min.js"></script>
<script src="/public/js/vendor/scrollreveal.min.js"></script>
<script src="/public/js/vendor/jquery.magnific-popup.min.js"></script>

<!-- Custom scripts for this template -->
<script src="/public/js/vendor/creative.min.js"></script>

<script src="/public/js/vendor/underscore.min.js"></script>
<script src="/public/js/vendor/backbone.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js"></script>
<script src="/public/js/vendor/select2.min.js"></script>

<script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
<script src="/public/js/selects.js"></script>
<script src="/public/js/views/Login.js"></script>
<script src="/public/js/views/Register.js"></script>
<script src="/public/js/index.js"></script>

<script>
    $(function() {
        $('#login-form-link').click(function(e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function(e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });

</script>

</body>

</html>

