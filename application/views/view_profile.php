<?php
/**
 * Created by PhpStorm.
 * User: MadalinaM
 * Date: 11/11/2017
 * Time: 4:07 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>


<?php
$first_name = "manu";
$last_name = "alina";

$this->db->select(['first_name', 'last_name', 'email']);
$this->db->where("id","1");
$result = $this->db->get('USERS')->result_array();
$result = $result[0];
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="/public/css/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="/public/css/magnific-popup.css" rel="stylesheet">
    <link href="/public/css/vendor/select2.min.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="/public/css/style.css" rel="stylesheet">
</head>
	<body>
	<div class="container events-container" id="events_container">
	    <div class="row">
	        <div class="col-lg-12">
	            <h3>EventizeR.</h3>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-lg-4 col-lg-offset-4">
	            Your Profile!
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <table class="table" id="events_list_table">
	                <thead>
	                <tr>
	                    <th>Last Name </th>
	                    <th><?php echo $result['first_name'] ?></th>
	                 </tr>
	                <tr>
	                    <th>First Name</th>
	                    <th><?php echo $result['last_name'] ?></th>
	                 </tr>
	                <tr>
	                    <th>Email</th>
	                    <th><?php echo $result['email'] ?></th>                    
	                 </tr>
	            </table>
	        </div>
	    </div>
	</div>

	</body>
</html>

