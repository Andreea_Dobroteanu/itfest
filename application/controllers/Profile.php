<?php
/**
 * Created by PhpStorm.
 * User: MadalinaM
 * Date: 11/11/2017
 * Time: 4:05 PM
 */

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_profile_model');
    }

    public function index($id = 1)
    {
        $data['first_name'] = $this->User_profile_model->get_user_first_name($id);
        $data['last_name'] = $this->User_profile_model->get_user_last_name($id);
        $data['profile_pic'] = $this->User_profile_model->get_user_profile_pic($id);
        $this->load->view('view_profile', $data);
    }

    public function edit($id = 1)
    {
        $data['first_name'] = $this->User_profile_model->get_user_first_name($id);
        $data['last_name'] = $this->User_profile_model->get_user_last_name($id);
        $this->load->view('edit_profile', $data);
    }

    public function submit($id = 1)
    {
       $res = $this->User_profile_model->edit_user_info(
           $id,
           $_POST['first_name'],
           $_POST['last_name'],
           $_POST
       );
       echo $res;

       $this->index($id);
    }
}