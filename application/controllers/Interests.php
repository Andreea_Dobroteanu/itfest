<?php
/**
 * Created by PhpStorm.
 * User: Metalbrain
 * Date: 11.11.2017
 * Time: 21:18
 */

class Interests extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->model('interests_model');
    }

    function index()
    {
        if ($this->session->userdata('logged_in')) {
            $data["auth"] = false;
        } else {
            /* not authentified */
            $data["auth"] = false;
        }


        $this->load->view('homepage', $data);
    }

    function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('logged_in');
        session_destroy();
        echo true;
    }


    function getAllInterests()
    {
        echo json_encode($this->interests_model->getAllInterests());
    }
}