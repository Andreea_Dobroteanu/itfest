<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->model('EventModel');
    }

    function index()
    {
        $data["events"] = $this->EventModel->getAllEvents();

        if ($this->session->userdata('logged_in')) {
            $data["auth"] = false;
        } else {
            /* not authentified */
            $data["auth"] = false;
        }


//        var_dump($data);


        $this->load->view('events', $data);
    }

	public function add() {
        $this->load->view('AddEventView');
    }

	/*add new event*/
	public function manage_post_add()
	{
        //TO DO: replace user with proper value
		$user = 1;
        $time = date("Y-m-d H:i:s");
        $res = $this->EventModel->event_add(
            $this->input->post('name'),
            $this->input->post('description'),
            $this->input->post('interest'),
            $this->input->post('date'),
            $this->input->post('location'),
            $time,
            $user
        );
        echo $res;
	}

	/*delete event by id*/
    public function manage_post_remove()
    {
        $res = $this->EventModel->event_remove(
            $this->input->post('id_event')
        );
        echo $res;
    }

    /*update event*/
    public function manage_post_update()
    {
        //TO DO: replace user with proper value
        $res = $this->EventModel->event_update(
            $_POST,
            $this->input->post('id')
        );
        echo $res;
    }

    public function get_users_events(){
        $id_user = 1;
        $res = $this->EventModel->get_events_for_user($id_user);
        echo $res;
    }
}

