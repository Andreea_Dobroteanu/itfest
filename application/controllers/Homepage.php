<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->model('user_profile_model');
    }

    function index()
    {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $id = $this->session->userdata('id');
            $data = $this->user_profile_model->get_user_first_name($id);
            if (empty($data)) {
                $data = ["auth" => true];
            } else {
                $data->auth = true;
            }
        } else {
            /* not authentified */
            $data["auth"] = false;
        }

        $this->load->view('homepage', $data);
    }

    function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('logged_in');
        session_destroy();
        echo true;
    }
}
