<?php

class Register extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('register_model');
    }

    public function index()
    {
    }

    public function register_user(){
        $errors = array();

        if(empty($this->input->post('first_name')) || empty($this->input->post('last_name'))
            || empty($this->input->post('email')) || empty($this->input->post('pass')))
        {
            array_push($errors, "Please fill all the fields");
        }

        if (empty($errors)) {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $pass = md5($this->input->post('pass'));
            $phone = $this->input->post('phone');
            $interests = $this->input->post('interests');

            $this->register_model->insert_profile($last_name, $first_name, $email, $pass, $phone, $interests);
        }

        header('Content-Type: application/json');
        echo json_encode($errors);
    }
}