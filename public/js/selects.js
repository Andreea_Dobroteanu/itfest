$("document").ready(function() {

    $.get("/interests/getAllInterests", function(interests) {

        interests = JSON.parse(interests);

        var parsedInterests = [];
        _.map(interests, function(interest) {
            parsedInterests.push({
                id: interest.id,
                text: interest.name
            });
        });

        $("#interests").select2({
            tags: parsedInterests
        });
    });
});
