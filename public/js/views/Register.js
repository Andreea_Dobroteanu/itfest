var Register = Backbone.View.extend({
    el: "#register-form",
    events: {
        "submit": "submitForm",
        "blur .form-control": "checkInput"
    },

    submitForm: function(e) {
        e.preventDefault();

        var base_elements = {
            first_name: this.$('#firstname'),
            last_name: this.$('#lastname'),
            email: this.$('#email'),
            pass: this.$('#password'),
            confpass: this.$('#confirm-password')
        };

        var errs = this.validateBaseElements(base_elements);

        if (!errs) {
            this.sendForm();
        }
    },

    sendForm: function(e) {
        var data = {
            first_name: this.$('#firstname').val(),
            last_name: this.$('#lastname').val(),
            email: this.$('#email').val(),
            pass: this.$('#password').val(),
            phone: this.$('#phone').val(),
            interests: JSON.stringify(this.$('#interests').val())
        };

        $.ajax({
            url: "register/register_user",
            method: "POST",
            data: data,
            dataType: "json",
            success: _.bind(function(err) {
                console.log ("Registered");
                if (!err.length) {

                    $(document).trigger("login", [data.email, data.pass]);

                }

                console.log(err);
            }, this),
            error: function() {
                console.log ("Error registering");
            }
        });
    },

    validateBaseElements: function(data) {
        var errors = [];
        Object.keys(data).map(_.bind(function(field) {
            if (!this.checkInput(data[field])) {
                errors.push(field);
            }
        }, this));

        !this.checkPassMatch() ? errors.push("confpass") : null;

        return errors.length;
    },

    checkInput: function(e) {
        var $el;
        if (e.length) {
            $el = e;
        } else {
            $el = $(e.target);
        }

        /* Mandatory fields */
        if ($el.hasClass('mandatory')) {
            if (!$el.val().length) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        /* Email check */
        if ($el.hasClass('email')) {
            if (!this.validateEmail($el.val())) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        /* Password strength */
        if ($el.hasClass('pass')) {
            if (!this.checkPassStrength($el.val())) {
                $el.addClass('error');
                return false;
            } else {
                $el.removeClass('error');
            }
        }

        if ($el.hasClass('confirm-pass')) {
            return this.checkPassMatch();
        }

        return true;
    },

    validateEmail: function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    checkPassStrength: function(pass) {
        return pass.trim().length > 7;
    },

    checkPassMatch: function() {
        var pass = this.$('.pass').val();
        var confPass = this.$('.confirm-pass').val();

        if (pass !== confPass) {
            this.$('.pass').addClass('error');
            this.$('.confirm-pass').addClass('error');
            return false;
        } else {
            this.$('.pass').removeClass('error');
            this.$('.confirm-pass').removeClass('error');
        }
        return true;
    }
});