var DeleteForm = Backbone.View.extend({
    job_to_leave: null,

    el: '#delete_job_modal',
    events: {
        "submit .delete-job-form": "confirmDelete",
        "click .cancel-delete": "cancelDelete"
    },

    initialize: function() {
        $(document).on("job_leave", _.bind(function(e, job_id) {
            this.job_to_leave = job_id;
        }, this));
    },

    cancelDelete: function() {
        this.$el.modal("hide");
    },

    confirmDelete: function(e) {
        e.preventDefault();

        var other_admin_id = this.$("#leave-job-select").val();
        var job_id = this.job_to_leave;

        $.ajax({
            url: "jobs/leave",
            method: "POST",
            dataType: "json",
            data: {
                job_id: job_id,
                other_admin_id: other_admin_id
            },
            success: _.bind(function() {
                console.log ("Passed to another one!");
                this.$el.modal("hide");
                location.reload();
            }, this),
            error: function() {
                console.log ("Error leaving it");
            }
        });
    }
});