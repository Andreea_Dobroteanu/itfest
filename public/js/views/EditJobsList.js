
var EditJobsList = Backbone.View.extend({
    el: '#admin_jobs_container',
    events: {
        "click .delete-job": "deleteJob",
        "click .comp-name": "editCompanyName",
        "click .job-descr": "editCompanyName",
        "blur .comp-name-edit": "submitCompNameInput",
        "keyup .comp-name-edit": "submitCompNameInputEnter",
        "blur .comp-descr-edit": "submitCompDescrInput",
        "keyup .comp-descr-edit": "submitCompNameDescrEnter"
    },

    initialize: function() {},

    editCompanyName: function(e) {
        if ($(e.target)[0].tagName === 'SPAN') {
            $(e.target).addClass("hidden");
            $(e.target).closest("td").find("input").removeClass("hidden");
        }
    },

    submitCompNameInputEnter: function(e) {
        if (e.which === 13) {
            $('.comp-name-edit').trigger("blur");
        }
    },

    submitCompNameDescrEnter: function(e) {
        if (e.which === 13) {
            $('.comp-descr-edit').trigger("blur");
        }
    },

    submitCompNameInput: function(e) {
        var comp_name = $(e.target).val();

        var job_id = parseInt($(e.target).closest('tr').find('.job-id-td').text(), 10);

        data = {
            company_name: comp_name,
            job_id: job_id
        };

        $.ajax({
            url: "jobs/edit_job_company_name",
            method: "POST",
            data: data,
            dataType: "json",
            success: _.bind(function(err) {
                $(e.target).addClass('hidden');
                $span = $(e.target).closest('td').find('span');
                $span.text($(e.target).val());
                $span.removeClass('hidden');
            }, this),
            error: function() {
                console.log ("Error adding job :(");
            }
        });
    },

    submitCompDescrInput: function(e) {
        var comp_name = $(e.target).val();

        var job_id = parseInt($(e.target).closest('tr').find('.job-id-td').text(), 10);

        data = {
            company_name: comp_name,
            job_id: job_id
        };

        $.ajax({
            url: "jobs/edit_job_company_descr",
            method: "POST",
            data: data,
            dataType: "json",
            success: _.bind(function(err) {
                $(e.target).addClass('hidden');
                $span = $(e.target).closest('td').find('span');
                $span.text($(e.target).val());
                $span.removeClass('hidden');
            }, this),
            error: function() {
                console.log ("Error adding job :(");
            }
        });
    },

    deleteJob: function (e) {
        var job_id = $(e.target).closest("tr").find(".job-id-td").text();
        $(document).trigger("job_leave", job_id);
    }
});