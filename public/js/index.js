(function () {
    // var landing = new Landing();
    var login = new Login();
    var register = new Register();

    $("#logout-link").on("click", function(e) {
        $.ajax({
            url: "homepage/logout",
            method: "GET",
            dataType: "json",
            success: _.bind(function() {
                $(".invalid-data").addClass("hidden");

                window.location.reload();

            }, this),
            error: function(res, err) {
                console.log("Fail logout");
            }
        });
    });

    $(".delete-event").on("click", function(e) {
        var id = $(e.target).data("id");

        $.ajax({
            url: "/event/manage_post_remove",
            method: "POST",
            data: {"id_event": id},
            dataType: "json",
            success: function() {

            }
        });

        $(e.target).closest("tr").remove();
    });

})($);