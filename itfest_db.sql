-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.20-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for itfest
CREATE DATABASE IF NOT EXISTS `itfest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `itfest`;

-- Dumping structure for table itfest.EVENTS
CREATE TABLE IF NOT EXISTS `EVENTS` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `id_owner` int(11) NOT NULL,
  `id_interest` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_location_event` (`id_location`),
  KEY `fk_interest_event` (`id_interest`),
  KEY `fk_owner` (`id_owner`),
  CONSTRAINT `fk_interest_event` FOREIGN KEY (`id_interest`) REFERENCES `INTERESTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_owner` FOREIGN KEY (`id_owner`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.EVENTS: ~0 rows (approximately)
/*!40000 ALTER TABLE `EVENTS` DISABLE KEYS */;
INSERT INTO `EVENTS` (`id`, `name`, `description`, `id_owner`, `id_interest`, `id_location`, `date`, `time`) VALUES
	(0, 'my name', 'my descp', 1, 1, 1, '2010-10-10', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `EVENTS` ENABLE KEYS */;

-- Dumping structure for table itfest.EVENT_MSG_BOARD
CREATE TABLE IF NOT EXISTS `EVENT_MSG_BOARD` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event` (`id_event`),
  KEY `fk_user_msg` (`id_user`),
  CONSTRAINT `fk_event_msg` FOREIGN KEY (`id_event`) REFERENCES `EVENTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_msg` FOREIGN KEY (`id_user`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.EVENT_MSG_BOARD: ~0 rows (approximately)
/*!40000 ALTER TABLE `EVENT_MSG_BOARD` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_MSG_BOARD` ENABLE KEYS */;

-- Dumping structure for table itfest.GROUPS
CREATE TABLE IF NOT EXISTS `GROUPS` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `members` int(11) NOT NULL,
  `interests` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_groups_interests` (`interests`),
  KEY `fk_groups_member` (`members`),
  CONSTRAINT `fk_groups_interests` FOREIGN KEY (`interests`) REFERENCES `INTERESTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_groups_member` FOREIGN KEY (`members`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.GROUPS: ~0 rows (approximately)
/*!40000 ALTER TABLE `GROUPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUPS` ENABLE KEYS */;

-- Dumping structure for table itfest.INTERESTS
CREATE TABLE IF NOT EXISTS `INTERESTS` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `proposed_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proposed_by` (`proposed_by`),
  CONSTRAINT `fk_proposed_by` FOREIGN KEY (`proposed_by`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.INTERESTS: ~1 rows (approximately)
/*!40000 ALTER TABLE `INTERESTS` DISABLE KEYS */;
INSERT INTO `INTERESTS` (`id`, `name`, `description`, `proposed_by`) VALUES
	(1, 'sdfs', 'thth', 1);
/*!40000 ALTER TABLE `INTERESTS` ENABLE KEYS */;

-- Dumping structure for table itfest.LOCATIONS
CREATE TABLE IF NOT EXISTS `LOCATIONS` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `contact_info` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `gps_position` varchar(100) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.LOCATIONS: ~1 rows (approximately)
/*!40000 ALTER TABLE `LOCATIONS` DISABLE KEYS */;
INSERT INTO `LOCATIONS` (`id`, `name`, `description`, `contact_info`, `address`, `gps_position`, `time`) VALUES
	(1, 'super', 'my location', 'contact location', 'location address', 'ugrgkjhjhwf', '2017-11-11 17:21:02');
/*!40000 ALTER TABLE `LOCATIONS` ENABLE KEYS */;

-- Dumping structure for table itfest.LOCATION_ACTIVITIES
CREATE TABLE IF NOT EXISTS `LOCATION_ACTIVITIES` (
  `id` int(11) NOT NULL,
  `id_location` int(11) DEFAULT NULL,
  `id_interest` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_location_activity` (`id_location`),
  KEY `fk_interest_location` (`id_interest`),
  CONSTRAINT `fk_interest_location` FOREIGN KEY (`id_interest`) REFERENCES `INTERESTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_location_activity` FOREIGN KEY (`id_location`) REFERENCES `LOCATIONS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.LOCATION_ACTIVITIES: ~0 rows (approximately)
/*!40000 ALTER TABLE `LOCATION_ACTIVITIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `LOCATION_ACTIVITIES` ENABLE KEYS */;

-- Dumping structure for table itfest.USERS
CREATE TABLE IF NOT EXISTS `USERS` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `profile_pic` longblob,
  `is_admin` bit(1) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.USERS: ~1 rows (approximately)
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` (`id`, `first_name`, `last_name`, `password`, `profile_pic`, `is_admin`, `timestamp`, `email`, `phone`) VALUES
	(1, 'user', 'uuuser', 'secret', NULL, b'0', '2017-11-11 17:19:59', NULL, NULL);
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;

-- Dumping structure for table itfest.USERS_EVENTS
CREATE TABLE IF NOT EXISTS `USERS_EVENTS` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_event` (`id_user`),
  CONSTRAINT `fk_user_event` FOREIGN KEY (`id_user`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.USERS_EVENTS: ~0 rows (approximately)
/*!40000 ALTER TABLE `USERS_EVENTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERS_EVENTS` ENABLE KEYS */;

-- Dumping structure for table itfest.USERS_GROUPS
CREATE TABLE IF NOT EXISTS `USERS_GROUPS` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_groups` (`id_user`),
  KEY `fk_user_groups_groups` (`id_group`),
  CONSTRAINT `fk_user_groups` FOREIGN KEY (`id_user`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_groups_groups` FOREIGN KEY (`id_group`) REFERENCES `GROUPS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.USERS_GROUPS: ~0 rows (approximately)
/*!40000 ALTER TABLE `USERS_GROUPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERS_GROUPS` ENABLE KEYS */;

-- Dumping structure for table itfest.USERS_INTERESTS
CREATE TABLE IF NOT EXISTS `USERS_INTERESTS` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_interest` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_interest` (`id_interest`),
  KEY `fk_user` (`id_user`),
  CONSTRAINT `fk_interest` FOREIGN KEY (`id_interest`) REFERENCES `INTERESTS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `USERS` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table itfest.USERS_INTERESTS: ~0 rows (approximately)
/*!40000 ALTER TABLE `USERS_INTERESTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERS_INTERESTS` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
